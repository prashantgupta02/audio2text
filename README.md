## Dev Setup ##

1 - Clone the repo from https://prashantgupta02@bitbucket.org/prashantgupta02/audio2text.git
2 - Open the command prompt and enter in audio2text folder.
3 - Run “npm install” command in the root folder to install the npm packages.
4 - Run “node script.js D:\Projects\audio2text\audio” command in the root folder to run where the "D:\Projects\audio2text\audio" is the directory path of all the audio files are available.
5 - It'll generate the doc files corressponding to each audio files in the same directory